#!/usr/bin/env python3

import sqlite3
from datetime import datetime

from flask_login import LoginManager, login_required, UserMixin
from flask import Flask, request, session, redirect, url_for, render_template, jsonify, flash, abort
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.secret_key = 'super_secret_ultra_important_key'

# define the database connection and cursor
conn = sqlite3.connect('chat.db', check_same_thread=False)
c = conn.cursor()

# route for the index page
@app.route('/')
def index():
    if 'username' not in session:
        return redirect(url_for('login'))
        
    user_id = session['user_id']  # assuming you have stored user_id in the session
    c.execute("SELECT * FROM messages")
    messages = c.fetchall()

    is_admin = session['username'] == 'admin'
    special_message = "OSCPPC{71f8f7c85af7311a491aef5a10789a9d}" if is_admin else ""

    return render_template('index.html', messages=messages, user_id=user_id, is_admin=is_admin, special_message=special_message)

# route for sending a message
@app.route('/send-message', methods=['POST'])
def send_message():
    if 'username' not in session:
        return redirect(url_for('login'))

    message = request.get_json()['message']
    sender = session['username']
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    c.execute("INSERT INTO messages (sender, message, timestamp) VALUES (?, ?, ?)", (sender, message, timestamp))
    conn.commit()
    return ('', 200)

# route for the login page
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        c.execute("SELECT * FROM users WHERE username=?", (username,))
        user = c.fetchone()
        if user and check_password_hash(user[2], password):
            session['user_id'] = user[0]
            session['username'] = user[1]
            return redirect(url_for('index'))
        else:
            error = 'Invalid username or password'
            return render_template('login.html', error=error)

    return render_template('login.html')

# route for the registration page
@app.route('/register', methods=['GET', 'POST'])
def register():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if len(username) < 3 or len(password) < 6:
            error = 'Username must be at least 3 characters and password must be at least 6 characters'
        else:
            c.execute("SELECT * FROM users WHERE username=?", (username,))
            row = c.fetchone()
            if row is not None:
                error = 'Username already exists'
            else:
                password_hash = generate_password_hash(password)
                c.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password_hash))
                conn.commit()
                return redirect(url_for('login'))

    return render_template('register.html', error=error)

# route for logout
@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))

# route for the profile page
@app.route('/profile/<int:user_id>', methods=['GET', 'POST'])
def profile(user_id):
    if 'username' not in session:
        return redirect(url_for('login'))

    c.execute("SELECT id, username, password FROM users WHERE id=?", (user_id,))
    user = c.fetchone()
    if not user:
        abort(404)
    if request.method == 'POST':
        password = request.form['password']
        password_hash = generate_password_hash(password)
        c.execute("UPDATE users SET password=? WHERE id=?", (password_hash, user_id))
        conn.commit()
        flash('Password changed successfully!')
        return redirect(url_for('index'))
    return render_template('profile.html', user=user, username=user[1])


# route for changing password
@app.route('/change_password/<int:user_id>', methods=['POST'])
def change_password(user_id):
    if 'username' not in session:
        return redirect(url_for('login'))

    c.execute("SELECT * FROM users WHERE id=?", (user_id,))
    user = c.fetchone()
    if not user:
        abort(404)

    new_password = request.form['new_password']
    confirm_password = request.form['confirm_password']

    if new_password != confirm_password:
        flash('New passwords do not match.')
        return redirect(url_for('profile', user_id=user_id))

    password_hash = generate_password_hash(new_password)
    c.execute("UPDATE users SET password=? WHERE id=?", (password_hash, user_id))
    conn.commit()
    flash('Password changed successfully!')
    return redirect(url_for('profile', user_id=user_id))

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
