#!/usr/bin/env python3

import sqlite3

conn = sqlite3.connect('chat.db')

cursor = conn.cursor()
cursor.execute('''CREATE TABLE users
                  (id INTEGER PRIMARY KEY AUTOINCREMENT,
                   username TEXT,
                   password TEXT)''')

cursor.execute('''CREATE TABLE messages
                  (id INTEGER PRIMARY KEY AUTOINCREMENT,
                   sender TEXT,
                   receiver TEXT,
                   message TEXT,
                   timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)''')

cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", ("admin", "123.qwe"))
cursor.execute("INSERT INTO messages (sender, receiver, message) VALUES (?, ?, ?)", ("admin", "admin", "Welcome to chat heaven"))

conn.commit()
