require 'sinatra/base'
require 'webrick'

class Exercise < Sinatra::Base
  helpers do
    alias_method :h, :escape_html
  end

  set :bind, '0.0.0.0'
  set :port, 8000
  set :views, File.join(File.dirname(__FILE__), 'views')

  get '/' do
    if params['username']
      @message = eval "\"Hello " + params['username'] + "\""
      return erb :index
    else
      redirect '/?username=hacker'
    end
  end

  def self.run!
    server_options = {
      Port: port,
      Host: bind
    }

    Rack::Handler::WEBrick.run Exercise, server_options do |server|
      [:INT, :TERM].each do |signal|
        trap(signal) { server.shutdown }
      end
    end
  end
end

Exercise.run! if $PROGRAM_NAME == __FILE__
