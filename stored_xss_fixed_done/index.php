<?php
session_start();
if (isset($_SESSION['username'])) {
    echo "<div class='navbar'>";
    echo "<a href='#' class='brand'>Stored XSS</a>";
    echo "<a href='logout.php' class='logout'>Logout</a>";
    echo "</div>";
    echo "<div class='container'>";
    echo "<h2>Welcome, " . $_SESSION['username'] . "!</h2>";

    if ($_SESSION['username'] === 'admin') {
        echo "<p>You are logged in as admin. Here is your flag: OSCPPC{c8b0a5a4494dc609120e09c6492b6074}</p>";
    }

    echo "<form action='comments.php' method='post'>
              <textarea name='comment' placeholder='Enter your comment'></textarea>
              <input type='submit' value='Post Comment'>
          </form>";

    // Update these variables with your database connection details
    $containerIP = trim(shell_exec("hostname -I | awk '{print $1}'"));
    $host = $containerIP;
    $database = 'stored_xss';
    $user = 'minhnq';
    $passwordDb = 'hackmeplease';

    // Establish a database connection
    $connection = new mysqli($host, $user, $passwordDb, $database);
    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }

    // Retrieve all comments from the database
    $query = "SELECT * FROM comments";
    $result = $connection->query($query);

    if ($result->num_rows > 0) {
        echo "<h2>Comments</h2>";
        while ($row = $result->fetch_assoc()) {
            $content = htmlspecialchars($row['content'], ENT_QUOTES, 'UTF-8');
            $username = htmlspecialchars($row['username'], ENT_QUOTES, 'UTF-8');
            $timestamp = $row['timestamp'];

            echo "<div class='comment'>";
            echo "<p><span class='username'>Username: </span>$username</p>";
            echo "<p><span class='comment-text'>Comment: </span>$content</p>";
            echo "<p><span class='timestamp'>Timestamp: </span>$timestamp</p>";
            echo "</div>";
        }
    } else {
        echo "<p>No comments found.</p>";
    }

    $connection->close();
    echo "</div>";
} else {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }

        .navbar {
            background-color: #f8f8f8;
            padding: 10px;
        }

        .brand {
            font-size: 24px;
            font-weight: bold;
        }

        .logout {
            float: right;
            text-decoration: none;
            color: #0000ff;
            margin-left: 10px;
        }

        .container {
            max-width: 800px;
            margin: 50px auto;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
            padding: 20px;
        }

        h2 {
            margin: 0;
        }

        textarea {
            width: 100%;
            height: 100px;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            font-size: 16px;
        }

        input[type="submit"] {
            background-color: #4caf50;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            font-size: 16px;
            cursor: pointer;
        }

        .comment {
            margin-bottom: 20px;
            padding: 10px;
            background-color: #f9f9f9;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        .username {
            font-weight: bold;
        }

        .comment-text {
            font-style: italic;
        }

        .timestamp {
            color: #999;
        }
    </style>
</head>
</html>
