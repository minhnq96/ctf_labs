<?php
session_start();

if (isset($_SESSION['username'])) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Update these variables with your database connection details
    $containerIP = trim(shell_exec("hostname -I | awk '{print $1}'"));
    $host = $containerIP;
    $database = 'stored_xss';
    $user = 'minhnq';
    $passwordDb = 'hackmeplease';

    // Establish a database connection
    $connection = new mysqli($host, $user, $passwordDb, $database);
    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }

    // Prepare and execute a SELECT query to check the username and password
    $stmt = $connection->prepare("SELECT username FROM users WHERE username = ? AND password = ?");
    $stmt->bind_param("ss", $username, $password);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
        // Valid login credentials
        $_SESSION['username'] = $username;
        header("Location: index.php");
        exit();
    } else {
        // Invalid login credentials
        $error = "Invalid username or password";
    }

    $stmt->close();
    $connection->close();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }
        
        .container {
            max-width: 400px;
            margin: 100px auto;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
            padding: 20px;
        }
        
        .container h2 {
            margin-top: 0;
        }
        
        .container input[type="text"],
        .container input[type="password"],
        .container input[type="submit"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            font-size: 16px;
        }
        
        .container input[type="submit"] {
            background-color: #4caf50;
            color: #fff;
            cursor: pointer;
        }
        
        .container input[type="submit"]:hover {
            background-color: #45a049;
        }
        
        .container p {
            margin-top: 20px;
        }
        
        .container p a {
            color: #0000ff;
        }
        
        .container .error {
            color: #ff0000;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Login</h2>
        
        <?php if (isset($error)) : ?>
        <p class="error"><?php echo $error; ?></p>
        <?php endif; ?>
        
        <form action="" method="post">
            <input type="text" name="username" placeholder="Username" required><br>
            <input type="password" name="password" placeholder="Password" required><br>
            <input type="submit" value="Login">
        </form>

        <p>Don't have an account? <a href="register.php">Register here</a>.</p>
    </div>
</body>
</html>
