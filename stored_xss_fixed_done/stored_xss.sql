CREATE DATABASE stored_xss;
USE stored_xss;

-- Table structure for table 'users'
CREATE TABLE `users` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
);

INSERT INTO `users` (`username`, `password`) VALUES
('admin', 'Super_p@ssw0rd');

-- Table structure for table 'comments'
CREATE TABLE `comments` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `content` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE USER 'minhnq'@'%' IDENTIFIED BY 'hackmeplease';
GRANT ALL PRIVILEGES ON stored_xss.* TO 'minhnq'@'%';
FLUSH PRIVILEGES;
