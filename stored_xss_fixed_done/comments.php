<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

if (isset($_POST['comment'])) {
    $comment = $_POST['comment'];
    $username = $_SESSION['username'];

    // Establish a database connection
    $containerIP = trim(shell_exec("hostname -I | awk '{print $1}'"));
    $host = $containerIP;
    $database = 'stored_xss';
    $user = 'minhnq';
    $passwordDb = 'hackmeplease';

    $connection = new mysqli($host, $user, $passwordDb, $database);
    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }

    // Store the comment in the database with the username and timestamp
    $stmt = $connection->prepare("INSERT INTO comments (content, username) VALUES (?, ?)");
    $stmt->bind_param("ss", $comment, $username);
    $stmt->execute();
    $stmt->close();

    $connection->close();

    header("Location: index.php");
    exit();
}

// Retrieve comments from the database and display them
// ...
?>
