<!DOCTYPE html>
<html>
<head>
    <title>My Cat Pics</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.7.2/dist/css/bootstrap.min.css">
    <style>
        body {
            background-color: #212529;
            color: #fff;
        }

        .image-card {
            text-align: center;
            padding: 20px;
            border-radius: 10px;
            background-color: #343a40;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
    </style>
</head>
<body>
    <div class="container py-5">
        <h1 class="text-center mb-4">My Favorite Cat Pics</h1>

        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
            <div class="col">
                <div class="card image-card">
                    <img src="/file.php?file=cat_1.png" class="card-img-top" alt="Cat 1">
                </div>
            </div>

            <div class="col">
                <div class="card image-card">
                    <img src="/file.php?file=cat_2.png" class="card-img-top" alt="Cat 2">
                </div>
            </div>

            <div class="col">
                <div class="card image-card">
                    <img src="/file.php?file=cat_3.png" class="card-img-top" alt="Cat 3">
                </div>
            </div>

            <div class="col">
                <div class="card image-card">
                    <img src="/file.php?file=cat_4.png" class="card-img-top" alt="Cat 4">
                </div>
            </div>

            <div class="col">
                <div class="card image-card">
                    <img src="/file.php?file=cat_5.png" class="card-img-top" alt="Cat 5">
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.7.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
