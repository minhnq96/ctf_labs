#!/usr/bin/env python3

from datetime import datetime
import mysql.connector
from flask import Flask, request, session, redirect, url_for, render_template, flash, abort
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__, static_folder='static')
app.secret_key = 'super_secret_ultra_important_key'

# Configure MySQL connection
mysql_config = {
    'user': 'minhnq',
    'password': 'H@ckm3pl3@53',
    'host': 'localhost',
    'database': 'airline',
    'raise_on_warnings': True
}
conn = mysql.connector.connect(**mysql_config)
cursor = conn.cursor()

# Route for the index page
@app.route('/')
def index():
    if 'user_id' in session:
        cursor.execute("SELECT * FROM tickets")
        tickets = cursor.fetchall()
        return render_template('index.html', tickets=tickets)
    else:
        return redirect(url_for('login'))

@app.route('/search_tickets', methods=['POST'])
def search_tickets():
    departure_city = request.form['departureCity']
    arrival_city = request.form['arrivalCity']
    departure_date = request.form['departureDate']

    tickets = perform_ticket_search(departure_city, arrival_city, departure_date)

    return render_template('index.html', tickets=tickets, search_executed=True)

def perform_ticket_search(departure_city, arrival_city, departure_date):
    conn = mysql.connector.connect(**mysql_config)
    cursor = conn.cursor()
    cursor.execute(f"SELECT * FROM tickets WHERE departure_city='{departure_city}' AND arrival_city='{arrival_city}' AND departure_date='{departure_date}';")
    tickets = cursor.fetchall()
    cursor.close()
    conn.close()
    return tickets

@app.route('/buy_ticket/<int:ticket_id>', methods=['GET'])
def buy_ticket(ticket_id):
    return redirect(url_for('index'))

# Route for the login page
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        cursor.execute("SELECT * FROM users WHERE username=%s", (username,))
        user = cursor.fetchone()
        if user and user[2] == password:
            session['user_id'] = user[0]
            session['username'] = user[1]
            return redirect(url_for('index'))
        else:
            error = 'Invalid username or password'
            return render_template('login.html', error=error)

    return render_template('login.html')

# Route for the registration page
@app.route('/register', methods=['GET', 'POST'])
def register():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        cursor.execute("SELECT * FROM users WHERE username=%s", (username,))
        row = cursor.fetchone()
        if row is not None:
            error = 'Username already exists'
        else:
            password_hash = generate_password_hash(password)
            cursor.execute("INSERT INTO users (username, password) VALUES (%s, %s)", (username, password_hash))
            conn.commit()
            return redirect(url_for('login'))

    return render_template('register.html', error=error)

# Route for logout
@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
