CREATE database airline;
USE airline;

CREATE TABLE `users` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(255),
    `password` VARCHAR(255));

INSERT INTO `users` (`username`, `password`) VALUES
('admin','OSCPPC{38ec3e8f8b1f0e3847062b3e106aa792}'),
('minh','123qwe'),
('johnny.bravo','password123'),
('perry','abc123'),
('wagner','qwerty'),
('sarah.conner','pass123');

CREATE TABLE `tickets` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `user_id` INT,
    `flight_number` VARCHAR(255),
    `departure_date` DATE,
    `departure_city` VARCHAR(255),
    `arrival_city` VARCHAR(255),
    `ticket_price` DECIMAL(10, 2),
    FOREIGN KEY (user_id) REFERENCES users (id)
);

INSERT INTO `tickets` (`user_id`, `flight_number`, `departure_date`, `departure_city`, `arrival_city`, `ticket_price`) VALUES
(1,'FL123','2023-05-22','New York','Los Angeles','250.00'),
(1,'FL456','2023-05-23','London','Paris','200.00'),
(2,'FL789','2023-05-24','Tokyo','Sydney','350.00'),
(2,'FL987','2023-05-25','Paris','Rome','250.00'),
(3,'FL654','2023-05-26','Los Angeles','New York','280.00'),
(3,'FL321','2023-05-27','Sydney','Tokyo','400.00'),
(4,'FL135','2023-05-28','Rome','London','220.00'),
(4,'FL642','2023-05-29','New York','Paris','300.00'),
(5,'FL753','2023-05-30','Paris','Tokyo','400.00'),
(5,'FL528','2023-05-31','London','New York','280.00');

CREATE USER 'minhnq'@'localhost' IDENTIFIED BY 'H@ckm3pl3@53';
GRANT ALL PRIVILEGES ON `airline`.* TO 'minhnq'@'localhost';
FLUSH PRIVILEGES;
