CREATE database sql_blind;
USE sql_blind;

CREATE TABLE `blogs` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(250) NOT NULL,
    `contents` VARCHAR(500) NOT NULL);

INSERT INTO `blogs` (`title`, `contents`) VALUES
('SQLi for dummy', 'SQL injection (SQLi) is a web security vulnerability that allows an attacker to interfere with the queries that an application makes to its database. It generally allows an attacker to view data that they are not normally able to retrieve. This might include data belonging to other users, or any other data that the application itself is able to access.'),
('SQLi error-based', 'Error-based SQLi is an in-band SQL Injection technique that relies on error messages thrown by the database server to obtain information about the structure of the database. In some cases, error-based SQL injection alone is enough for an attacker to enumerate an entire database.'),
('SQLi time-based', 'Time-based SQL Injection is an inferential SQL Injection technique that relies on sending an SQL query to the database which forces the database to wait for a specified amount of time (in seconds) before responding.');

CREATE TABLE `secret` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `flag` VARCHAR(250) NOT NULL);

INSERT INTO `secret` (`flag`) VALUES ('OSCPPC{645fca75f51fc4f8a87de2aba376dbff}');

CREATE USER 'minhnq'@'%' IDENTIFIED BY 'hackmeplease';
GRANT ALL PRIVILEGES ON `sql_blind`.* TO 'minhnq'@'%';
FLUSH PRIVILEGES;
