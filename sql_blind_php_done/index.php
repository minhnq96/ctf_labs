<?php
// Establish database connection
$host = "localhost";
$username = "minhnq";
$password = "hackmeplease";
$database = "sql_blind";

$conn = new mysqli($host, $username, $password, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Handle search form submission
if (isset($_GET['search'])) {
    $searchId = $_GET['search'];
    $query = "SELECT * FROM blogs WHERE id = $searchId";
} else {
    $query = "SELECT * FROM blogs"; // Query to retrieve all blog posts
}

// Retrieve blog posts from the database
$result = $conn->query($query);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Blog Posts</title>
    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css">
    <style>
        /* Custom CSS styles (optional) */
        body {
            padding-top: 2rem;
        }

        .container {
            max-width: 800px;
        }

        h1 {
            margin-bottom: 2rem;
            text-align: center;
        }

        form {
            margin-bottom: 2rem;
            text-align: center;
        }

        ul {
            list-style-type: none;
            padding: 0;
        }

        li {
            margin-bottom: 2rem;
        }

        h2 {
            margin-bottom: 1rem;
            font-size: 24px;
        }

        p {
            color: #777;
        }

        .back-link {
            display: block;
            margin-top: 1rem;
            color: #007bff;
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Blog Posts</h1>

        <!-- Search form -->
        <form action="" method="GET">
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search by ID">
                <button class="btn btn-primary" type="submit">Search</button>
            </div>
        </form>

        <!-- Display blog post titles -->
        <?php if ($result->num_rows > 0) : ?>
            <ul>
                <?php while ($row = $result->fetch_assoc()) : ?>
                    <li>
                        <h2><?php echo $row['title']; ?></h2>
                        <p><?php echo $row['contents']; ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php else : ?>
            <p>No blog posts found.</p>
        <?php endif; ?>

        <a class="back-link" href="/">Back to Blog Posts</a>
    </div>

    <!-- Include Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
