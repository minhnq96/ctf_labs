#!/bin/bash

service mysql start
sleep 5
mysql < /sql_blind.sql
rm /sql_blind.sql
rm /var/www/html/index.html
php -S 0.0.0.0:80 /var/www/html/index.php

while true; do sleep 1; done;
