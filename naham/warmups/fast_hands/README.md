# Fast Hands

- Just read source code of the web and go to

```bash
http://challenge.nahamcon.com:31565/capture_the_flag.html
```

- Then read source code and get flag

```bash
flag{80176cdf1547a9be54862df3568966b8}
```
