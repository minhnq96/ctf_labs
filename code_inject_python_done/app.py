from flask import Flask,redirect,render_template 

app = Flask(__name__)

@app.route('/hello/<user>')
def hello_name(user):
	message = eval('"Hello '+user+'"')
	return render_template('index.html', message = message)

@app.route('/')
def hello_world():
	return redirect("/hello/hacker", code=302)

if __name__ == '__main__':
	app.run(debug=False,host='0.0.0.0')