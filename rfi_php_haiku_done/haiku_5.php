<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f7f7f7;
        padding: 20px;
        margin: 0;
    }

    pre {
        font-size: 18px;
        line-height: 1.6;
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        white-space: pre-wrap;
        text-align: justify;
        max-width: 800px;
        margin: 0 auto;
    }
</style>
<?php
// Your poem content
$poem = <<<EOD
From ashes, reborn,
Phoenix soaring through the flames,
Renewal's embrace.
EOD;

// Display the poem
echo "<pre>$poem</pre>";
?>
