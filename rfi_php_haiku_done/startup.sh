#!/bin/bash

sed -i 's/allow_url_include = Off/allow_url_include = On/' /etc/php/7.4/apache2/php.ini
service apache2 start
rm /var/www/html/index.html

while true; do sleep 1; done;
