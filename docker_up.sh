#!/bin/bash

for dir in $(ls -d */); do
    echo "Running docker-compose in $dir"
    (cd $dir && docker-compose up -d)
done
