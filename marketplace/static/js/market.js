// Get the dropdown button and content
var dropdownButton = document.querySelector(".dropbtn");
var dropdownContent = document.querySelector(".dropdown-content");

// When the button is clicked, toggle the "show" class for the content
dropdownButton.addEventListener("click", function() {
  dropdownContent.classList.toggle("show");
});

// When the user clicks outside of the dropdown, close it
window.addEventListener("click", function(event) {
  if (!event.target.matches(".dropbtn")) {
    dropdownContent.classList.remove("show");
  }
});
