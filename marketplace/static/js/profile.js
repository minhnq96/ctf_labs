document.addEventListener('DOMContentLoaded', function() {
  // Send a GET request to the server to retrieve the user's information
  fetch('/user', { method: 'GET' })
    .then(response => response.json())
    .then(user => {
      // Update the username in the HTML
      const usernameElement = document.getElementById('username');
      usernameElement.innerText = user.username;
    })
    .catch(error => console.error(error));

  // Add an event listener to the logout form to submit a GET request to the server to log out the user
  const logoutForm = document.getElementById('logout-form');
  logoutForm.addEventListener('submit', function(event) {
    event.preventDefault();

    fetch('/logout', { method: 'GET' })
      .then(() => {
        // Redirect the user to the login page
        window.location.href = '/';
      })
      .catch(error => console.error(error));
  });
});
