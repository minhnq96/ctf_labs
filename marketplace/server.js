#!/usr/bin/env node

const md5 = require('md5');
const express = require('express');
const flash = require('connect-flash');
const bodyParser = require('body-parser');
const session = require('express-session');
const sqlite3 = require('sqlite3').verbose();

const port = 3000;
const app = express();
const db = new sqlite3.Database('market.db');

app.set('view engine', 'ejs');
app.use(flash());
app.use(express.static('static'));
app.use(bodyParser.urlencoded({ extended: true }));

// Initialize session middleware
app.use(session({
  secret: 'mysecret',
  resave: false,
  saveUninitialized: true
}));

// Middleware to check if user is authenticated
const isAuthenticated = (req, res, next) => {
  if (req.session.authenticated) {
    // User is authenticated, continue to the next middleware
    next();
  } else {
    // User is not authenticated, redirect to the login page
    res.redirect('/');
  }
};

app.get('/', (req, res) => {
  res.render('index', { message: null, registerMessage: null });
});

app.post('/login', async (req, res) => {
  const emailOrUsername = req.body.emailOrUsername;
  const password = md5(req.body.password);

  db.get('SELECT * FROM users WHERE (username = ? OR email = ?) AND password = ?', [emailOrUsername, emailOrUsername, password], (err, row) => {
    if (err || !row) {
      // Redirect to the index.html page with an error message
      res.redirect('/?error=Invalid%20username%20or%20password');
    } else {
      // Set the authenticated property in the session object
      req.session.authenticated = true;
      // Redirect to the market route upon successful login
      res.redirect('/market');
    }
  })
});

app.get('/register', function(req, res) {
  res.render('register', { error: req.flash('registerMessage') });
});

app.post('/register', (req, res) => {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;

  if (password !== confirmPassword) {
    // Passwords do not match, redirect back to the register page with an error message
    res.render('register', { error: 'Passwords do not match' });
  } else {
    // Check if a user with the same email or username already exists
    db.get('SELECT * FROM users WHERE username = ? OR email = ?', [username, email], (err, row) => {
      if (err || row) {
        // User already exists, redirect back to the register page with an error message
        res.render('register', { error: 'User with the same username or email already exists' });
      } else {
        // Insert the new user into the database
        const hashedPassword = md5(password);
        db.run('INSERT INTO users (username, email, password) VALUES (?, ?, ?)', [username, email, hashedPassword], (err) => {
          if (err) {
            // Error inserting the user into the database, redirect back to the register page with an error message
            res.render('register', { error: 'Error registering the user. Please try again later.' });
          } else {
            // User successfully registered, redirect to the index page with a success message
            res.render('index', { message: req.flash('User successfully registered') });
          }
        });
      }
    });
  }
});

const products = [ // replace this with your products data
  {
    name: 'Audi A3',
    price: 36495,
    description: 'The A3 has everything we like about modern Audis, shrunk down to size small.',
    vendor: 'Audi',
    image: './img/products/audi_a3.jpg'
  },
  {
    name: 'Audi A4',
    price: 41395,
    description: 'The A4 is exceptionally well-rounded and effortlessly competent, but sexy it’s not.',
    vendor: 'Audi',
    image: './img/products/audi_a4.jpg'
  },
  {
    name: 'Audi A5',
    price: 47795,
    description: 'The two-door A5 is like a better-looking A4 sedan, but with less rear seat space.',
    vendor: 'Audi',
    image: './img/products/audi_a5.jpg'
  },
  {
    name: 'Audi A6',
    price: 57995,
    description: 'Neither breathtaking to drive nor to look at, the A6 nonetheless delivers on its promise of luxury—and offers a high-tech user experience as a bonus.',
    vendor: 'Audi',
    image: './img/products/audi_a6.jpg'
  },
];

// Route for /market accessible only by authenticated users
app.get('/market', isAuthenticated, (req, res) => {
  // Render the market page for authenticated users
  // Add an id property to each product object based on its index in the array
  products.forEach((product, index) => {
    product.id = index;
  });

  res.render('market', { username: req.session.username, products });
});

// add the following route to handle GET requests to /logout
app.get('/logout', (req, res) => {
  // destroy the session
  req.session.destroy((err) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect('/');
    }
  });
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
