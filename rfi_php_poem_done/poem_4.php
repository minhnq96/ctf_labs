<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f7f7f7;
        padding: 20px;
        margin: 0;
    }

    pre {
        font-size: 18px;
        line-height: 1.6;
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        white-space: pre-wrap;
        text-align: justify;
        max-width: 800px;
        margin: 0 auto;
    }
</style>
<?php
// Your poem content
$poem = <<<EOD
In the silence of the night, I'm searching for a sign,
Lost in the echoes of my mind, a melody divine.
Whispers in the breeze, they dance upon the air,
Guiding me through shadows, leading me to where...

Stars collide, as we dream tonight,
Our souls entwined, in this cosmic light.
Weaving tales of love, with each word we speak,
A symphony of hearts, where our spirits seek.

Through the valleys and the mountains, we'll find our way,
Painting colors on the canvas of a brand new day.
Through the rivers and the oceans, we'll navigate,
Embracing the rhythm of a shared fate.

Like a phoenix rising from the ashes,
We'll soar above the doubts and clashes.
With every note and every rhyme,
Our song will stand the test of time.

Stars collide, as we dream tonight,
Our souls entwined, in this cosmic light.
Weaving tales of love, with each word we speak,
A symphony of hearts, where our spirits seek.

As the moon shines on, our voices intertwine,
Leaving echoes in the night, melodies divine.
Forever we'll be bound, in this harmonious embrace,
A song that echoes through eternity, filling every space.
EOD;

// Display the poem
echo "<pre>$poem</pre>";
?>
