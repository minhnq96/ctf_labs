<!DOCTYPE html>
<html>
<head>
    <title>My Poems Collections</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .navbar {
            background-color: #f1f1f1;
            padding: 10px;
            display: flex;
            justify-content: center;
        }

        .navbar a {
            padding: 10px;
            text-decoration: none;
            color: #333;
            margin: 0 5px;
        }

        .content {
            max-width: 800px;
            margin: 0 auto;
            text-align: center;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: flex-start;
            min-height: 80vh;
        }

        .header {
            margin-bottom: 40px;
        }

        footer {
            background-color: #f1f1f1;
            padding: 20px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="content">
        <div class="header">
            <h1>Welcome to My Poems Collections</h1>
        </div>

        <nav class="navbar">
            <a href="/index.php">Home</a>
            <a href="/index.php?page=poem_1.php">Poem 1</a>
            <a href="/index.php?page=poem_2.php">Poem 2</a>
            <a href="/index.php?page=poem_3.php">Poem 3</a>
            <a href="/index.php?page=poem_4.php">Poem 4</a>
            <a href="/index.php?page=poem_5.php">Poem 5</a>
        </nav>

        <?php
        // Get the page name from the query parameter
        $page = $_GET["page"];

        // Check if the page parameter is set and not empty
        if (isset($page) && !empty($page)) {
            // Include the specified page
            include $page;
        } else {
            echo "<p>Select a poem from the navigation bar.</p>";
        }
        ?>
    </div>

    <footer>
        <p>Thank you for visiting!</p>
    </footer>
</body>
</html>
