<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f7f7f7;
        padding: 20px;
        margin: 0;
    }

    pre {
        font-size: 18px;
        line-height: 1.6;
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        white-space: pre-wrap;
        text-align: justify;
        max-width: 800px;
        margin: 0 auto;
    }
</style>
<?php
// Your poem content
$poem = <<<EOD
In the realm of charm and sweet delight,
Where laughter dances and hearts take flight,
I find myself drawn to your radiant smile,
A captivating presence that makes my heart beguile.

Your eyes, like stars, sparkle with a mischievous gleam,
They speak a language only we dare to dream.
In their depths, secrets of desire unfold,
A tantalizing story waiting to be told.

Your laughter, like music, fills the air,
A melody that entices, beyond compare.
It dances on the wind, enchanting and free,
Drawing me closer, like a magnet, you see.

In every word you speak, a flirtatious tease,
A playful banter that puts my heart at ease.
Your wit and charm, a delightful embrace,
Creating a connection I yearn to chase.

With every touch, a subtle electric sensation,
A gentle brush that ignites fiery temptation.
In the dance of fingertips, our bodies speak,
A language of desire, both tender and sleek.

So let us indulge in this flirtatious game,
A delightful chase, where hearts set aflame.
For in the realm of flirtation, passion's bloom,
An enchanting journey where love finds its room.
EOD;

// Display the poem
echo "<pre>$poem</pre>";
?>
