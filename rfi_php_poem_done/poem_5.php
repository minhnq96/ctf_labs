<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f7f7f7;
        padding: 20px;
        margin: 0;
    }

    pre {
        font-size: 18px;
        line-height: 1.6;
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        white-space: pre-wrap;
        text-align: justify;
        max-width: 800px;
        margin: 0 auto;
    }
</style>
<?php
// Your poem content
$poem = <<<EOD
In a time of ancient battles, where heroes dared to roam,
A tale of valor and courage, to be forever known.
A warrior strong and noble, with a heart so bold and true,
A champion of justice, with a destiny to pursue.

With sword in hand, he ventured forth, through perils dark and grim,
Facing foes and challenges, with a fearless, unwavering vim.
His armor gleamed like starlight, his eyes ablaze with fire,
A beacon of hope in the midst of chaos and dire.

Across treacherous landscapes, he bravely forged his way,
Confronting monstrous creatures, who threatened the light of day.
He battled dragons with their fiery breath, and giants fierce and tall,
Defying the odds, he stood his ground, never to falter or fall.

Through haunted forests and haunted nights, his spirit remained unbroken,
Rescuing the oppressed, mending what was torn and broken.
His name echoed through the lands, a symbol of strength and might,
A legend etched in the annals, a beacon shining bright.

But his heroism ran deeper, beyond the battles fought,
For within his noble heart, compassion and kindness were sought.
He aided those in need, lifting the downtrodden and weak,
A true hero, not just in deed, but in the values he'd always seek.

So let us raise our voices, in honor of this noble soul,
Whose deeds inspire us all, making us stronger and whole.
In this heroic tale, we find the power to believe,
That within each of us lies a hero, waiting to achieve.
EOD;

// Display the poem
echo "<pre>$poem</pre>";
?>
