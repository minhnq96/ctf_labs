<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f7f7f7;
        padding: 20px;
        margin: 0;
    }

    pre {
        font-size: 18px;
        line-height: 1.6;
        background-color: #fff;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        white-space: pre-wrap;
        text-align: justify;
        max-width: 800px;
        margin: 0 auto;
    }
</style>
<?php
// Your poem content
$poem = <<<EOD
In a land of laughter and cheer,
I'll tell you a poem, have no fear.
It's filled with whimsy and delight,
So let's embark on this jolly flight.

Once there was a dancing cow,
Who waltzed with grace, somehow.
She twirled and spun across the field,
With moves that made the flowers yield.

A squirrel joined the joyful spree,
Hopping around from tree to tree.
He chattered and laughed with glee,
As he swung from branch to branch so free.

A clumsy bear stumbled near,
His steps were far from clear.
He tripped and tumbled, rolling down,
Turning everyone's smile into a frown.

But the cow and squirrel were quick to aid,
They laughed and helped the bear upgrade.
Together they danced a silly jig,
And laughed so hard, they did a wig.

The animals danced till the moon grew bright,
Their laughter echoed into the night.
For in this world of playful jest,
Joy and mirth will be our quest.

So let us embrace each funny tale,
And let laughter and smiles prevail.
For life's too short to be serious all the way,
So let's keep chuckling, day by day!
EOD;

// Display the poem
echo "<pre>$poem</pre>";
?>
