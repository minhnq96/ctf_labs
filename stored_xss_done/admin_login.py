#!/usr/bin/env python3

import time
import subprocess

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service

# Specify the path to the geckodriver executable
driver_path = '/var/www/html/geckodriver'

# Create a new Service instance
service = Service(executable_path=driver_path, log_path='geckodriver.log')

# Create Firefox options and set headless mode
options = Options()
# options.headless = True
options.add_argument("--headless")

# Create a new instance of Firefox WebDriver
driver = webdriver.Firefox(service=service, options=options)

# Get the IP address of the Docker container
result = subprocess.run(["hostname", "-I"], capture_output=True, text=True)
output = result.stdout.strip()
ip_address = output.split()[0]

# Open the webpage
driver.get(f'http://{ip_address}/login.php')

# Find the username and password fields and login button using name attributes
username_field = driver.find_element(By.NAME, 'username')
password_field = driver.find_element(By.NAME, 'password')
login_button = driver.find_element(By.CSS_SELECTOR, 'input[type="submit"][value="Login"]')

# Enter the login credentials
username_field.send_keys('admin')
password_field.send_keys('Super_p@ssw0rd')

# Click the login button
login_button.click()

# Handle JavaScript alert
try:
    alert = driver.switch_to.alert
    alert.accept()
except:
    pass

time.sleep(5)  # Adjust the delay as needed

# Close the browser
driver.quit()
