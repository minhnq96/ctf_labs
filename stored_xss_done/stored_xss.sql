CREATE DATABASE stored_xss;
USE stored_xss;

-- Table structure for table 'users'
CREATE TABLE `users` (
  `id` int AUTO_INCREMENT PRIMARY KEY, -- Add a primary key 'id'
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL DEFAULT 'user' -- Add a role field with default value 'user'
);

INSERT INTO `users` (`username`, `password`, `role`) VALUES
('super_admin', 'SuperSecret@2024', 'super_admin'),
('admin', 'Super_p@ssw0rd', 'admin');

-- Table structure for table 'comments'
CREATE TABLE `comments` (
  `id` int AUTO_INCREMENT PRIMARY KEY,
  `content` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_private` tinyint(1) NOT NULL DEFAULT 0 -- Add a field to indicate if a comment is private
);

INSERT INTO `comments` (`content`, `username`, `is_private`) VALUES
('blog1{9e6a2293958470c41898fcfd5738b653}', 'admin', 1),
('blog2{c4b367f5143aaff2db82f498370365c8}', 'super_admin', 1),
('Important: super_admin:31337_P@ssw0rd@123', 'super_admin', 1);

CREATE USER 'minhnq'@'%' IDENTIFIED BY 'hackmeplease';
GRANT ALL PRIVILEGES ON stored_xss.* TO 'minhnq'@'%';
FLUSH PRIVILEGES;
