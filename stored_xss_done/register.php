<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $role = 'user'; // Set the default role as 'user'

    // Update these variables with your database connection details
    $containerIP = trim(shell_exec("hostname -I | awk '{print $1}'"));
    $host = $containerIP;
    $database = 'stored_xss';
    $user = 'minhnq';
    $passwordDb = 'hackmeplease';

    // Establish a database connection
    $connection = new mysqli($host, $user, $passwordDb, $database);
    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }

    // Validate and process the registration form
    // You can implement additional validation and security measures here
    $stmt = $connection->prepare("INSERT INTO users (username, password, role) VALUES (?, ?, ?)");
    $stmt->bind_param("sss", $username, $password, $role);

    if ($stmt->execute()) {
        // Registration successful
        $stmt->close();
        $connection->close();

        // Redirect the user to the login page
        header("Location: login.php");
        exit();
    } else {
        // Registration failed
        echo "Error: " . $stmt->error;
    }

    $stmt->close();
    $connection->close();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }
        
        .container {
            max-width: 400px;
            margin: 100px auto;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
            padding: 20px;
        }
        
        .container h2 {
            margin-top: 0;
        }
        
        .container input[type="text"],
        .container input[type="password"],
        .container input[type="submit"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            font-size: 16px;
        }
        
        .container input[type="submit"] {
            background-color: #4caf50;
            color: #fff;
            cursor: pointer;
        }
        
        .container input[type="submit"]:hover {
            background-color: #45a049;
        }
        
        .container p {
            margin-top: 20px;
        }
        
        .container p a {
            color: #0000ff;
        }
        
        .container .error {
            color: #ff0000;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h2>Register</h2>
        
        <?php if (isset($error)) : ?>
        <p class="error"><?php echo $error; ?></p>
        <?php endif; ?>
        
        <form action="" method="post">
            <input type="text" name="username" placeholder="Username" required><br>
            <input type="password" name="password" placeholder="Password" required><br>
            <input type="submit" value="Register">
        </form>

        <p>Already have an account? <a href="login.php">Login here</a>.</p>
    </div>
</body>
</html>
