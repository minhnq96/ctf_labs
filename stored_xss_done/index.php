<?php
session_start();
if (isset($_SESSION['username'])) {
    echo "<div class='navbar'>";
    echo "<a href='#' class='brand'>Blog Exam1</a>";
    echo "<a href='logout.php' class='logout'>Logout</a>";
    echo "</div>";
    echo "<div class='container'>";
    echo "<h2>Welcome, " . htmlspecialchars($_SESSION['username']) . "!</h2>";

    echo "<form action='comments.php' method='post'>
              <textarea name='comment' placeholder='Enter your comment'></textarea><br>";

    // Only admins and super_admins can post private comments
    if ($_SESSION['role'] === 'admin' || $_SESSION['role'] === 'super_admin') {
        echo "<input type='checkbox' name='is_private' value='1'> Mark as private<br>";
    }

    echo "<input type='submit' value='Post Comment'>
          </form>";

    // Placeholders for comments
    echo "<div id='comments-section'></div>";

    // Pass user_id and role to JavaScript
    echo "<script>
            const userId = '" . $_SESSION['user_id'] . "';
            const role = '" . $_SESSION['role'] . "';
          </script>";
} else {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 0;
        }

        .navbar {
            background-color: #f8f8f8;
            padding: 10px;
        }

        .brand {
            font-size: 24px;
            font-weight: bold;
        }

        .logout {
            float: right;
            text-decoration: none;
            color: #0000ff;
            margin-left: 10px;
        }

        .container {
            max-width: 800px;
            margin: 50px auto;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
            padding: 20px;
        }

        h2 {
            margin: 0;
        }

        textarea {
            width: 100%;
            height: 100px;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            font-size: 16px;
        }

        input[type="submit"] {
            background-color: #4caf50;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            font-size: 16px;
            cursor: pointer;
        }

        .comment {
            margin-bottom: 20px;
            padding: 10px;
            background-color: #f9f9f9;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        .username {
            font-weight: bold;
        }

        .comment-text {
            font-style: italic;
        }

        .timestamp {
            color: #999;
        }

        .private {
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            fetch(`comments.php?user_id=${userId}`)
                .then(response => response.json())
                .then(data => displayComments(data))
                .catch(error => console.error('Error fetching comments:', error));
        });

        function displayComments(comments) {
            const commentsSection = document.getElementById('comments-section');
            if (comments.length > 0) {
                commentsSection.innerHTML = '<h2>Comments</h2>';
                comments.forEach(comment => {
                    const commentDiv = document.createElement('div');
                    commentDiv.classList.add('comment');

                    const commenter = document.createElement('p');
                    commenter.innerHTML = `<span class="username">Username: </span>${comment.username}`;

                    const content = document.createElement('p');
                    content.innerHTML = `<span class="comment-text">Comment: </span>${comment.content}`;

                    const timestamp = document.createElement('p');
                    timestamp.innerHTML = `<span class="timestamp">Timestamp: </span>${comment.timestamp}`;

                    commentDiv.appendChild(commenter);
                    commentDiv.appendChild(content);
                    commentDiv.appendChild(timestamp);

                    if (comment.is_private && (role === 'admin' || role === 'super_admin')) {
                        const privateLabel = document.createElement('p');
                        privateLabel.classList.add('private');
                        privateLabel.textContent = 'Private Comment';
                        commentDiv.appendChild(privateLabel);
                    }

                    commentsSection.appendChild(commentDiv);
                });
            } else {
                commentsSection.innerHTML = '<p>No comments found.</p>';
            }
        }
    </script>
</body>
</html>
