<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
}

// Update these variables with your database connection details
$containerIP = trim(shell_exec("hostname -I | awk '{print $1}'"));
$host = $containerIP;
$database = 'stored_xss';
$user = 'minhnq';
$passwordDb = 'hackmeplease';

// Establish a database connection
$connection = new mysqli($host, $user, $passwordDb, $database);
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Handle POST request to post a comment
    $comment = $_POST['comment'];
    $username = $_SESSION['username'];
    $is_private = 0; // Default to public comment

    // Only admin and super_admin can post private comments
    if (isset($_POST['is_private']) && ($_SESSION['role'] === 'admin' || $_SESSION['role'] === 'super_admin')) {
        $is_private = 1;
    }

    // Prepare the SQL query to insert the comment
    $stmt = $connection->prepare("INSERT INTO comments (content, username, is_private) VALUES (?, ?, ?)");
    $stmt->bind_param("ssi", $comment, $username, $is_private);
    $stmt->execute();
    $stmt->close();
    $connection->close();

    header("Location: index.php");
    exit();
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Handle GET request to retrieve comments
    if (isset($_GET['user_id'])) {
        $user_id = $_GET['user_id'];
        $role = $_SESSION['role'];  // Get the role from the session

        // Prepare the SQL query to retrieve comments
        if ($role === 'admin' || $role === 'super_admin') {
            // Fetch both public and private comments for the given user_id
            $query = "SELECT * FROM comments WHERE username = (SELECT username FROM users WHERE id = ?) OR is_private = 0";
        } else {
            // Regular users can only see public comments
            $query = "SELECT * FROM comments WHERE is_private = 0 AND username = (SELECT username FROM users WHERE id = ?)";
        }

        $stmt = $connection->prepare($query);
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $result = $stmt->get_result();

        $comments = array();
        while ($row = $result->fetch_assoc()) {
            $comments[] = $row;
        }

        $stmt->close();
        $connection->close();

        // Return the comments as JSON
        header('Content-Type: application/json');
        echo json_encode($comments);
    } else {
        // If the required parameters are not present, return an error
        header('HTTP/1.1 400 Bad Request');
        echo json_encode(['error' => 'Missing required parameters.']);
    }
}
?>
