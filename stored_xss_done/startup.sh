#!/bin/bash

sed -i 's/bind-address/bind-address = 0.0.0.0 #/' /etc/mysql/mysql.conf.d/mysqld.cnf
service cron start
service mysql start
service apache2 start
sleep 5
mysql < /var/www/html/stored_xss.sql
rm /var/www/html/sql_blind.sql
rm /var/www/html/index.html

while true; do sleep 1; done;
