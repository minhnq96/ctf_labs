#!/bin/bash

echo '<FilesMatch "\.php\.">
    SetHandler application/x-httpd-php
</FilesMatch>
' >> /etc/apache2/sites-available/000-default.conf

echo '<FilesMatch "\.php\.">
    SetHandler application/x-httpd-php
</FilesMatch>
' >> /etc/apache2/apache2.conf

echo '<FilesMatch "\.php\.">
    ForceType application/x-httpd-php
    SetHandler application/x-httpd-php
</FilesMatch>
' >> /var/www/html/.htaccess

a2enmod mime
a2enmod setenvif

service apache2 start

rm /var/www/html/index.html

chown www-data: -R /var/www/

while true; do sleep 1; done;
