<!DOCTYPE html>
<html>
<head>
    <title>File Upload</title>
</head>
<body>
    <?php
    $allowedExtensions = array('jpg', 'jpeg', 'png', 'gif'); // Allowed file extensions

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $fileExtension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

        // Check if the file extension is allowed
        if (!in_array($fileExtension, $allowedExtensions)) {
            echo 'File extension not allowed.';
        } else {
            // Upload the file
            $targetDirectory = 'uploads/';
            $targetFile = $targetDirectory . basename($_FILES['file']['name']);

            if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) {
                echo 'File uploaded successfully.';
            } else {
                echo 'Error uploading the file.';
            }
        }
    }
    ?>

    <form method="POST" enctype="multipart/form-data">
        <input type="file" name="file" />
        <input type="submit" value="Upload" />
    </form>

    <h3>Uploaded Files:</h3>
    <ul>
        <?php
        // Display uploaded files
        $files = glob('uploads/*');

        foreach ($files as $file) {
            $fileExtension = pathinfo($file, PATHINFO_EXTENSION);

            // Exclude disallowed file extensions
            if (!in_array($fileExtension, $allowedExtensions)) {
                continue;
            }

            echo '<li><a href="' . $file . '"><img src="' . $file . '" width="150" height="150"></a></li>';
        }
        ?>
    </ul>
</body>
</html>
