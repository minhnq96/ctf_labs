const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send(`
    <!DOCTYPE html>
    <html>
    <head>
      <title>Greetings!</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    <body>

      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Reflected XSS App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home</a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container mt-5">
        <div class="row">
          <div class="col-sm-12 col-md-6 offset-md-3">
            <div class="card">
              <div class="card-header">
                <h2>Greetings!</h2>
              </div>
              <div class="card-body">
                <form method="POST" action="/hello">
                  <div class="form-group">
                    <label for="name">Enter your name:</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                  </div>
                  <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@2.9.3/dist/umd/popper.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
    </body>
    </html>
  `);
});

app.post('/hello', (req, res) => {
  const { name } = req.body;
  const url = `/hello/${name}`;
  res.redirect(url);
});

app.get('/hello/:name', (req, res) => {
  const { name } = req.params;
  const html = `
    <html>
      <head>
        <style>
          /* Center the page content */
          body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
          }

          /* Style the greeting */
          h1 {
            font-size: 3rem;
            color: #333;
            text-align: center;
            margin-bottom: 2rem;
          }

          /* Style the container for the name */
          .name-container {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 10rem;
            background-color: #f7f7f7;
            border-radius: 1rem;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
            padding: 1rem;
            margin-bottom: 2rem;
          }

          /* Style the name */
          .name {
            font-size: 2rem;
            font-weight: bold;
            color: #333;
          }
        </style>
      </head>
      <body>
        <div>
          <h1>Hello, ${name}!</h1>
          <div class="name-container">
            <span>Welcome! To the Reflected XSS Website</span>
          </div>
        </div>
      </body>
    </html>
  `;
  res.send(html);
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log(`Listening on PORT ${PORT}`)
})
