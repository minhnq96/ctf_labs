<?php
if (isset($_GET["file"])) {
    $file = $_GET["file"];
    $file = str_replace(array('../'), '', $file); // Remove '.' and '/' characters
    $filePath = 'images/' . $file;

    if (file_exists($filePath)) {
        $contentType = mime_content_type($filePath);
        header('Content-Type: ' . $contentType);
        readfile($filePath);
    } else {
        die("File not found");
    }
} else {
    die("File not found");
}
?>
