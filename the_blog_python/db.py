#!/usr/bin/env python3

import sqlite3

# Connect to the database or create it if it doesn't exist
conn = sqlite3.connect('blog.db')

# Create a cursor object to execute SQL commands
cursor = conn.cursor()

# Create the User table
cursor.execute('''
    CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT UNIQUE NOT NULL,
        password TEXT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )
''')

# Create the BlogPost table
cursor.execute('''
    CREATE TABLE IF NOT EXISTS blog_post (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        content TEXT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        user_id INTEGER NOT NULL,
        FOREIGN KEY (user_id) REFERENCES users (id)
    )
''')

# Commit the changes and close the connection
conn.commit()
conn.close()
