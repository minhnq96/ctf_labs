#!/usr/bin/env python3

import base64
import sqlite3
import requests

from flask import Flask, render_template, request, redirect, url_for, session, flash, jsonify
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.secret_key = 'your_secret_key_?_you_mean_my_secret_key_?'

# define the database connection and cursor
conn = sqlite3.connect('blog.db', check_same_thread=False)
c = conn.cursor()

# Route for login page
@app.route('/login', methods=['GET'])
def login():
    return render_template('login.html')

# Route for handling login action
@app.route('/api/login', methods=['POST'])
def handle_login():
    username = request.form['username']
    password = request.form['password']

    # Retrieve user from the database based on username
    c.execute('SELECT * FROM users WHERE username = ?', (username,))
    user = c.fetchone()

    if user and check_password_hash(user[2], password):
        session['user_id'] = user[0]

        # Generate and store Authorization-Token
        token = base64.b64encode(f"{username}:{password}".encode('utf-8')).decode('utf-8')
        session['token'] = token

        return redirect(url_for('api_login'))  # Redirect to the API login route
    else:
        flash('Invalid username or password.', 'error')
        return redirect(url_for('login'))

# Route for the actual API login
@app.route('/api/login', methods=['GET'])
def api_login():
    # Perform any necessary tasks for API login
    # ...

    return redirect(url_for('index'))

# Route for registration page
@app.route('/register', methods=['GET'])
def register():
    return render_template('register.html')

# Route for handling registration action
@app.route('/api/register', methods=['POST'])
def handle_register():
    username = request.form['username']
    password = request.form['password']
    confirm_password = request.form['confirm_password']

    # Check if the passwords match
    if password != confirm_password:
        flash('Passwords do not match.', 'error')
        return redirect(url_for('register'))

    # Check if the username is already taken
    c.execute('SELECT * FROM users WHERE username = ?', (username,))
    existing_user = c.fetchone()

    if existing_user:
        flash('Username is already taken.', 'error')
        return redirect(url_for('register'))

    # Create a new user in the database
    hashed_password = generate_password_hash(password)
    c.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, hashed_password))
    conn.commit()

    flash('Registration successful! You can now log in.', 'success')
    return redirect(url_for('login'))

# Route for the index page
@app.route('/')
def index():
    if 'user_id' in session:
        c.execute('''
            SELECT blog_post.id, blog_post.title, blog_post.content, blog_post.created_at, users.username
            FROM blog_post
            INNER JOIN users ON blog_post.user_id = users.id
            ORDER BY blog_post.created_at DESC
        ''')
        blogs = c.fetchall()
        print(blogs[1][1])
        return render_template('index.html', blogs=blogs)
    else:
        return redirect(url_for('login'))

# Route for getting user information
@app.route('/api/user/<int:user_id>', methods=['GET'])
def get_username(user_id):
    if 'Authorization-Token' not in request.headers:
        return {'error': 'Authorization-Token is missing'}, 401

    token = request.headers['Authorization-Token']
    # Decode Authorization-Token to get username and password
    decoded_token = base64.b64decode(token).decode('utf-8')
    username, password = decoded_token.split(':')

    # Check if the username and password are valid
    c.execute('SELECT * FROM users WHERE username = ?', (username,))
    user = c.fetchone()

    if not user or not check_password_hash(user[2], password):
        return {'error': 'Invalid username or password'}, 401

    # Retrieve the username from the database
    user = c.execute('SELECT username FROM users WHERE id = ?', (user_id,)).fetchone()
    if user:
        return jsonify({'username': user[0]})
    else:
        return jsonify({'error': 'User not found'}), 404

# API route to get blog post details
@app.route('/api/blog/<int:blog_id>')
def get_blog(blog_id):
    blog = c.execute('''
        SELECT blog_post.id, blog_post.title, blog_post.content, blog_post.created_at, users.username
        FROM blog_post
        INNER JOIN users ON blog_post.user_id = users.id
        WHERE blog_post.id = ?
    ''', (blog_id,)).fetchone()
    if blog:
        comments = c.execute('''
            SELECT comment.content, comment.created_at, users.username
            FROM comment
            INNER JOIN users ON comment.user_id = users.id
            WHERE comment.blog_post_id = ?
            ORDER BY comment.created_at ASC
        ''', (blog_id,)).fetchall()
        return jsonify({
            'id': blog['id'],
            'title': blog['title'],
            'content': blog['content'],
            'created_at': blog['created_at'],
            'author': blog['username'],
            'comments': [{'content': c['content'], 'created_at': c['created_at'], 'username': c['username']} for c in comments]
        })
    return jsonify(error="Blog not found"), 404

# Route for logout
@app.route('/logout')
def logout():
	session.clear()
	return redirect(url_for('login'))

# Route for displaying the new blog form
@app.route('/new_blog', methods=['GET'])
def new_blog_form():
    if 'user_id' in session:
        return render_template('new_blog.html')
    else:
        return redirect(url_for('login'))

# Route for creating a new blog
@app.route('/api/create_blog', methods=['POST'])
def create_blog():
    if 'Authorization-Token' not in request.headers:
        return {'error': 'Authorization-Token is missing'}, 401

    token = request.headers['Authorization-Token']
    # Decode Authorization-Token to get username and password
    decoded_token = base64.b64decode(token).decode('utf-8')
    username, password = decoded_token.split(':')

    # Check if the username and password are valid
    c.execute('SELECT * FROM users WHERE username = ?', (username,))
    user = c.fetchone()

    if not user or not check_password_hash(user[2], password):
        return {'error': 'Invalid username or password'}, 401

    title = request.form['title']
    content = request.form['content']
    user_id = session['user_id']

    # Insert the new blog into the database
    c.execute('INSERT INTO blog_post (title, user_id, content) VALUES (?, ?, ?)', (title, user_id, content))
    conn.commit()

    flash('New blog created successfully!', 'success')
    return redirect(url_for('index'))

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
